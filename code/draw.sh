exp=$1

cp -r $1 ../../results_fig_generator
cd ../../results_fig_generator
bash prepare_data.sh $1
bash prepare_heat.sh
bash prepare_scatter.sh
bash prepare_scatter.sh
mkdir figures
rm figures/*
rm *.png figures/

